import * as moment from 'moment';

export class DateFormatValueConverter {
  toView(value) {
    console.log(value)
    if(typeof value =="string")
      {
        let from = value.split('/');
        let newDate =  new Date(parseInt(from[2]), parseInt(from[1])-1,parseInt(from[0]));
        return moment(newDate).format('DD/MM/YYYY');
      }
    else{
      return moment(value).format('DD/MM/YYYY');
    }
  }
}
export class DateTimeFormatValueConverter {
  toView(value) {
      if(value)
        {
          if(typeof value != "object" && (value.split('/')).length>1)
          {
            let from = value.split('/');
            let newDate =  new Date([from[2],from[1],from[0]].join('/'));
            return moment(newDate).format('DD/MM/YYYY HH:mm');
          }
          else
            return moment(value).format('DD/MM/YYYY HH:mm');
        }
  }
}
