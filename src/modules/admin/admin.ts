import { AuthenService } from './../../authen/authenService';
import { PLATFORM } from 'aurelia-pal';

import { inject } from 'aurelia-dependency-injection';
import { Aurelia } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import "../../helpers/loggingSetting";
import "../../helpers/axiosClient"
// import "../../helpers/axiosInterceptor";
@inject(AuthenService, Router)
export class App {
  router: Router;
  userInfo: any;
  constructor(private authenSrv: AuthenService) {
    this.userInfo = this.authenSrv.userInfo;

  }
  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Aurelia';
    config.map([
      {
        route: 'dashboard', name: 'dashboard', moduleId: PLATFORM.moduleName('../dashboard/index'), nav: true, title: 'Dashboard',
        settings: { icon: 'pg-home' }
      },


      // Quản Ly Dau Viec
      ,
      {
        route: ['', 'quan-ly-dau-viec'], name: 'quan-ly-dau-viec', moduleId: PLATFORM.moduleName('../quan-ly-dau-viec/index'), nav: true, title: 'Quản lý đầu việc',
        settings: { icon: 'pg-tables' }
      },
      {
        route: 'quan-ly-ke-hoach', name: 'quan-ly-ke-hoach', moduleId: PLATFORM.moduleName('../quan-ly-ke-hoach/index'), nav: true, title: 'Quản lý kế hoạch',
        settings: { icon: 'pg-home' }
      },
    ]);

    this.router = router;

  }
  attached() {
    var script = document.createElement("script");
    script.src = "assets/scripts.js";
    script.type = "text/javascript";
    document.getElementsByTagName("head")[0].appendChild(script);
  }
}
