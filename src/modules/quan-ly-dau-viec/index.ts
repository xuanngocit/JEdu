import { PLATFORM } from 'aurelia-pal';
import { childViewer } from './../../helpers/child-viewer';
import { inlineView } from 'aurelia-templating';
import { inject } from 'aurelia-dependency-injection';
import { Router, RouterConfiguration } from 'aurelia-router';
import { logger } from "./logger";
@inlineView(childViewer)
export class QuanLyDauViec {
    router: Router;
    heading = 'Quản lý đầu việc';
    configureRouter(config: RouterConfiguration, router: Router) {
        config.map([
            { route: ['', 'quan-ly-dau-viec'], name: 'quan-ly-dau-viec', moduleId: PLATFORM.moduleName('./quan-ly-dau-viec'), nav: true, title: 'Quản lý đầu việc' }]);
        this.router = router;
        logger.debug('router', this.router)
    }
}