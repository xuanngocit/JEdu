import {PLATFORM} from 'aurelia-pal';
import {InsertOrUpdateDauViec} from './dialogs/dau-viec-dialog';
import {DauViec} from './models/dau-viec';
import {Filter} from './../../resources/base/filter-base';
import {ViewModelBase} from './../../resources/base/viewmodel-base';
import {inject, autoinject} from 'aurelia-framework';
import {logger} from "./logger";
import {DialogService} from "aurelia-dialog";
@inject(DialogService)
export class Quanlydauviec implements ViewModelBase {
    items : any[];
    selectedItem : any;
    selectedList : any[];
    filter : any;
    constructor(private dialogService : DialogService) {}
    runFilter() {
        throw new Error("Method not implemented.");
    }
    runCreate() {
        //torun gan select tu dialog tra ve
        this.selectedItem = new DauViec()
        this
            .dialogService
            .open({viewModel: InsertOrUpdateDauViec, model: this.selectedItem})
            .whenClosed((result) => {
                if (!result.wasCancelled) {
                    this.selectedItem = result.output;

                } else {
                    logger.info("Cancel");
                }
            });

        logger.info("runCreate()", this.selectedItem)
        
    }
    runUpdate(item : any) {
        throw new Error("Method not implemented.");
    }
    runDelete(id : any) {
        throw new Error("Method not implemented.");
    }
    runDeleteMany(ids : any) {
        throw new Error("Method not implemented.");
    }

}
