import { ValidationRules } from 'aurelia-validation';
export class DauViec {
  id: number
  ma: number
  ten: string
  constructor() {
  }
}
export const DauViecValidationRules = ValidationRules
  .ensure((i: DauViec) => i.ma).required()
  .ensure(i => i.ten).required()
  .on(DauViec).rules