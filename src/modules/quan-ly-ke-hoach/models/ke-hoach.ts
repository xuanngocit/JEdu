import { ValidationRules } from 'aurelia-validation';
export class KeHoach {
    Id: number;
    Projectname:  string;
    MaKeHoach:string
    MaYeuCau:string
    MieuTa:string
    PersionCreatecreate:number
    Details:string
    
    // Ngoai firebase thì không cần mapdata trong contrustor này
    constructor() {
    }
    
}
export const KeHoachValidationRules = ValidationRules
.ensure((i: KeHoach) => i.Id).required()
.ensure(i => i.Projectname).required()
.on(KeHoach).rules
// http://thuthuat.taimienphi.vn/lam-quen-voi-asp-net-core-va-angular-4-thong-qua-web-api-27601n.aspx
// http://www.c-sharpcorner.com/article/creating-web-api-using-code-first-approach-in-entity-framework/
// https://docs.microsoft.com/en-us/aspnet/core/data/ef-rp/intro
// https://www.codeproject.com/Articles/1209903/Getting-Started-with-Entity-Framework-Core-Databas
// https://code.msdn.microsoft.com/How-to-using-Entity-1464feea
// https://docs.microsoft.com/en-us/ef/core/what-is-new/
