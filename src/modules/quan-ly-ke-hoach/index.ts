import { PLATFORM } from 'aurelia-pal';
import { childViewer } from './../../helpers/child-viewer';
import { inlineView } from 'aurelia-templating';
import { inject } from 'aurelia-dependency-injection';
import { Router, RouterConfiguration } from 'aurelia-router';
import { logger } from "./logger";
@inlineView(childViewer)
export class QuanLyKeHoach {
  router: Router;
  heading = 'Quản lý kế hoạch';
  configureRouter(config: RouterConfiguration, router: Router) {
    // trong 1 cai config.mapp thoi
    config.map([
      { route: ['', 'ke-hoach'], name: 'ke-hoach', moduleId: PLATFORM.moduleName('./ke-hoach'), nav: true, title: 'Kế hoạch' } // [''] la default route cho module
    
    ]);

    this.router = router;
    logger.debug('router', this.router)
  }
}
