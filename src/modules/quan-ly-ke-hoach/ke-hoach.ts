
// import { Filter } from './../../resources/base/filter-base';
// import { ViewModelBase } from './../../resources/base/viewmodel-base'; import { inject } from 'aurelia-framework';
// import { logger } from "./logger";
// import { DialogService } from "aurelia-dialog";
// import { FakeQuanLyGiamDinhServiceImpl, QuanLyGiamDinhService } from './services/quan-ly-giam-dinh-service';
// import { QuanLyGiamDinh } from './models/quan-ly-giam-dinh';
// import { InsertOrViewDS } from './dialogs/view-dialog';
// import { InsertOrUpdateDS } from './dialogs/chinh-sua-dialog';
// import { ChiTietGiamDinh } from './models/chi-tiet';
// import { PLATFORM } from 'aurelia-pal';
// import { InsertOrLyDo } from './dialogs/ly-do-dialog';
// import { InsertOrChapNhan } from './dialogs/chap-nhan-dialog';
// import { InsertOrChiDinh } from './dialogs/chi-dinh-dialog';
// import { InsertOrThemHoSo } from './dialogs/them-ho-so-dialog';
//@inject(FakeQuanLyGiamDinhServiceImpl, DialogService)
// export class YeuCau implements ViewModelBase {
//   [x: string]: any;
//   items: any[];
//   itemsCount: number
//   selectedItem: any;
//   selectedList: any[];
//   filter = { skip: 0, limit: 10, where: {} }
//   asyncTask // task control waiting view


//    constructor(private yeuCauSrv: QuanLyGiamDinhService, private dialogService: DialogService) {

//     }

//   async activate(params, routeConfig, navigationInstruction) {
//     await this.runFilter()

//   }


//   async runFilter() {
//    logger.info('runFilter', this.filter)
//     await (this.asyncTask = Promise.all([
//       this.yeuCauSrv.GetAll(this.filter).then(rec => this.items = rec),
//       this.yeuCauSrv.GetCount(this.filter).then(rec => this.itemsCount = rec),
//       // this.timerDo(1000) 
//     ]))

//   }

//   async runCreate() {
//     //torun gan select tu dialog tra ve
//     this.selectedItem = new ChiTietGiamDinh()
//     this.dialogService.open({ viewModel: InsertOrUpdateDS, model: this.selectedItem }).whenClosed((result) => {
//         if (!result.wasCancelled) {
//             this.selectedItem = result.output;
//             this.yeuCauSrv.Post(this.selectedItem).then(_ => this.showSuccess()).then(_ => this.runFilter()).catch(err => this.showError(err))
//         } else {
//             logger.info("Cancel");
//         }
//     });

//     logger.info("runCreate()", this.selectedItem)
//     //this.yeuCauSrv.Post(this.selectedItem)
//   }

//   async runView() {
//     //torun gan select tu dialog tra ve
//     this.selectedItem = new ChiTietGiamDinh()
//     this.dialogService.open({ viewModel: InsertOrViewDS, model: this.selectedItem }).whenClosed((result) => {
//         if (!result.wasCancelled) {
//             this.selectedItem = result.output;
//             this.yeuCauSrv.Post(this.selectedItem).then(_ => this.showSuccess()).then(_ => this.runFilter()).catch(err => this.showError(err))
//         } else {
//             logger.info("Cancel");
//         }
//     });

//     logger.info("runView()", this.selectedItem)
//     //this.yeuCauSrv.Post(this.selectedItem)
//   }

//   runViewLyDo(){
//     throw new Error("Method not implemented.");
//   }
//   async runUpdate(dsyeucauDto) {
//     //torun gan select tu dialog tra ve
//     this.selectedItem = dsyeucauDto
//     logger.info("runUpdate()", this.selectedItem)
//     this.dialogService.open({ viewModel: InsertOrUpdateDS, model: this.selectedItem }).whenClosed((result) => {
//       if (!result.wasCancelled) {
//         this.selectedItem = result.output;
//         this.yeuCauSrv.Put(this.selectedItem).then(_ => this.showSuccess()).then(_ => this.runFilter()).catch(err => this.showError(err))
//       } else {
//         logger.info("Cancel");
//       }
//     });
//   }

//   runChiDInhGDV(chiDinhDto) {
//     this.selectedItem = chiDinhDto
//     logger.info("runChapNhan()", this.selectedItem)
//     this.dialogService.open({ viewModel: InsertOrChiDinh, model: this.selectedItem }).whenClosed((result) => {
//       if (!result.wasCancelled) {
//         this.selectedItem = result.output;
//         this.yeuCauSrv.Put(this.selectedItem).then(_ => this.showSuccess()).then(_ => this.runFilter()).catch(err => this.showError(err))
//       } else {
//         logger.info("Cancel");
//       }
//     });
//   }

//   runChapNhan(chapNhanDto) {
//     this.selectedItem = chapNhanDto
//     logger.info("runChapNhan()", this.selectedItem)
//     this.dialogService.open({ viewModel: InsertOrChapNhan, model: this.selectedItem }).whenClosed((result) => {
//       if (!result.wasCancelled) {
//         this.selectedItem = result.output;
//         this.yeuCauSrv.Put(this.selectedItem).then(_ => this.showSuccess()).then(_ => this.runFilter()).catch(err => this.showError(err))
//       } else {
//         logger.info("Cancel");
//       }
//     });
//   }

//   runTuChoi(viewlydoDto) {
//     this.selectedItem = viewlydoDto
//     logger.info("runTuChoi()", this.selectedItem)
//     this.dialogService.open({ viewModel: InsertOrLyDo, model: this.selectedItem }).whenClosed((result) => {
//       if (!result.wasCancelled) {
//         this.selectedItem = result.output;
//         this.yeuCauSrv.Put(this.selectedItem).then(_ => this.showSuccess()).then(_ => this.runFilter()).catch(err => this.showError(err))
//       } else {
//         logger.info("Cancel");
//       }
//     });
//   }

//   runYeuCauThucHien() {
//     throw new Error("Method not implemented.");
//   }

//   async runDelete(dsyeucauDto) {
//     logger.info("runDelete()", dsyeucauDto)
//     await
//       this.confirm(result => {
//           if (result) this.yeuCauSrv.Delete(dsyeucauDto.id).then(_ => this.showSuccess()).then(_ => this.runFilter())
//           else this.showCancel()

//       })
//   }
//   // runThemHoSo(themHoSoDto) {
//   //   this.selectedItem = themHoSoDto
//   //   logger.info("runTuChoi()", this.selectedItem)
//   //   this.dialogService.open({ viewModel: InsertOrThemHoSo, model: this.selectedItem }).whenClosed((result) => {
//   //     if (!result.wasCancelled) {
//   //       this.selectedItem = result.output;
//   //       this.yeuCauSrv.Put(this.selectedItem).then(_ => this.showSuccess()).then(_ => this.runFilter()).catch(err => this.showError(err))
//   //     } else {
//   //       logger.info("Cancel");
//   //     }
//   //   });
//   // }

//   runDeleteMany() {
//     throw new Error("Method not implemented.");
//   }

//   //private
//    private showSuccess() {
//         PLATFORM.global.swal("Thành công", "Thực hiện thành công", "success");
//     }
//     private showError(err) {
//         PLATFORM.global.swal("Không thành công", `${err}`, "error");
//     }
//     private showCancel() {
//         PLATFORM.global.swal("Đã hủy", "Đã hủy thao tác", "warning");
//     }
//     private confirm(cb) {
//         PLATFORM.global.swal({
//           title: "Are you sure?",
//           text: "You will not be able to recover this imaginary file!",
//           type: "warning",
//           showCancelButton: true,
//           confirmButtonColor: "#DD6B55",
//           confirmButtonText: "Yes, delete it!",
//           cancelButtonText: "No, cancel plx!",
//           closeOnConfirm: false,
//           closeOnCancel: false
//       },
//           function (isConfirm) {
//               cb(isConfirm)
//           })
//     }

// }
// default
import { Filter } from './../../resources/base/filter-base';
import { ViewModelBase } from './../../resources/base/viewmodel-base';
import { KeHoachService } from './services/ke-hoach-service';
import { inject } from 'aurelia-framework';
import { logger } from "./logger";
import { DialogService } from "aurelia-dialog";
import { PLATFORM } from 'aurelia-pal';
import * as firebase from 'firebase';

@inject(KeHoachService)
export class kehoach implements ViewModelBase {
  items: any[];
  selectedItem: any;
  selectedList: any[];
  filter: any;
  asyncTask
  //Các func Default của framework
  private db = firebase.database();
  constructor(private kethoachSrv: KeHoachService, private dialogService: DialogService) {
    console.log(this.db)

  }
  async activate() {
    this.runFilter()

    //  await this.runFilter()
  }
  // Các func được implements tại đây nhé, Default là các func chính. 
  async runFilter() {
    await (this.asyncTask = Promise.all([
      this.kethoachSrv.GetAll().then((rec: any) => {
      this.items = rec
      })]))
  }
  runCreate() {
    throw new Error("Method not implemented.");
  }
  runUpdate(item: any) {
    throw new Error("Method not implemented.");
  }
  runDelete(id: any) {
    throw new Error("Method not implemented.");
  }
  runDeleteMany(ids: any) {
    throw new Error("Method not implemented.");
  }
  // function riêng tại đây
}
