import { Filter } from './../../../resources/base/filter-base';
import { BaseService } from './../../../resources/base/service-base';
import axios from 'axios';
import { KeHoach } from '../models/ke-hoach';
import * as firebase from 'firebase';
import * as _ from 'lodash';
import { AppSetting } from '../../../appsettings/index'
export class KeHoachService implements BaseService {
  private db = firebase.database();

  Get(id: number): Promise<any> {
    throw new Error("Method not implemented.");
  }
 async GetAll(): Promise<any[]> {
   // Firebase realtime 
  //   return new Promise((resolve, reject) => {
  //     let keHoachs: KeHoach[] = [];
  //     this.db.ref('/Project').once('value').then(function (snapshot) {
  //         snapshot.forEach(function (childSnapshot) {
  //             var childKey = childSnapshot.key;
  //             var childData = childSnapshot.val();
  //             let kh = new KeHoach(_.assignIn({ Id: childKey }, childData));
  //             keHoachs.push(kh);
       
  //             resolve(keHoachs);
  //         });
  //     }, err => reject(err));
  // })
      let rec = await axios.get('api/CongViecs', {
        baseURL: AppSetting.apiEndPoint
    })
    return rec.data
  }
  GetCount(filter?: Filter): Promise<number> {
    throw new Error("Method not implemented.");
  }
  Post(item: any): Promise<any> {
    throw new Error("Method not implemented.");
  }
  Put(item: any): Promise<any> {
    throw new Error("Method not implemented.");
  }
  Delete(id: number): Promise<any> {
    throw new Error("Method not implemented.");
  }
  DeleteMany(Ids: number[]): Promise<any> {
    throw new Error("Method not implemented.");
  }

}

