import { KeHoach, KeHoachValidationRules } from './../models/ke-hoach';
import { logger } from './../logger'; 
import { BootstrapFormRenderer } from './../../../helpers/bootstrap-form-renderer';
import { inject } from 'aurelia-framework';
import { DialogController } from "aurelia-dialog";
import { ValidationControllerFactory, ValidationController } from "aurelia-validation";
@inject(DialogController, ValidationControllerFactory)

export class InsertOrUpdateKeHoach {
  validationcontroller: ValidationController;
  constructor(private ctrl: DialogController, private controllerFactory) {
    this.validationcontroller = controllerFactory.createForCurrentScope();
    this.validationcontroller.addRenderer(new BootstrapFormRenderer());
  }
  get getTieuDe() {
    if (this.item.Id) return "Cập nhật";
    return "Thêm mới";
  }
  item: KeHoach;
  activate(dto: KeHoach) {
    this.item = dto
    logger.info('KeHoach dto', dto);
     
  }
  save() {
    // logger.info('item', this.item)
    // this.validationcontroller.validate({ object: this.item, rules: phongBanValidationRules }).then((result) => {
    //   logger.info('result.valid', result.valid)
    //   if (result.valid) {
    //     this.ctrl.ok(this.item);
    //   }
    // })

  }

}
